##  docker compose file builder 

An **simple** app to generate a docker-compose.yml file.

Just for fun.

The aim to create this app is to replace the generate a docker compose file according to the placeholder, sometime we need to set out config file according to dynamic environment. To be more Convenience,we can make it more easy to config according the environment.

for now, I just set the local IP dynamicly.

It's Very suitable for avoiding cross origin when use webpack in a project without separated from backend.


### How to use 

#### First

```
    yarn add dockerComposeFileBuilder   //or
    npm i dockerComposeFileBuilder
```

#### Second

```
    const path = require('path');
    const GenerateDockerComposeFile = require('docke-compose-builder');

    const generateDockerComposeFile = new GenerateDockerComposeFile({
        dockerComposeRoot:path.resolve(__dirname,'docker/compose/file/should/place'),
        dockerComposeTemplateFilePath:path.resolve(__dirname,'where/your/docker/compose/template/located.ext')
    })
    generateDockerComposeFile.start();

```

#### Third

template file demo

```
    version: "3"
    services:
        nginx:
            image: nginx
            expose:
                - "8080"
            volumes:
                - ./nginx/conf.d/mysite.template:/etc/nginx/conf.d/mysite.template
            environment:
                - NGINX_LOCALMACHINE_IP=###LOCAL_IP###
            extra_hosts:
            - localhost:127.0.0.1
            command: /bin/bash -c "envsubst '$${NGINX_LOCALMACHINE_HOST}'< /etc/nginx/conf.d/mysite.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"
```

#### Four

nginx config template

```
    upstream webpack_server {
        server ${NGINX_LOCALMACHINE_HOST};
    }

    server {
        listen 80;
        server_name demo.com;
        autoindex on;
        root /var/www;

        location / {
            try_files $uri $uri/ @webpackserver;
        }
        location @webpackserver {
            proxy_pass http://webpack_server;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            add_header Data-Form "webpackserver";
        }
    }
```

##### We can also use it in our webpack configuration

for example

```
    devServer: {
        host: "0.0.0.0",
        port: 8080,
        hot: false,
        historyApiFallback: true,
        disableHostCheck: true,
        before:function(){
            generateDockerComposeFile.start();
        }
    }
```
when we start the webpack it can generate the docker-compose.yml file and run it. Therefore we can open the browser and view the page without worrying about the cross origin issue.