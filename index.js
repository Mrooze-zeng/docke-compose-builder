const path = require("path");
const {readFileSync,writeFileSync} = require("fs");
const {networkInterfaces} = require("os");
const {execFileSync} = require("child_process")

class DockerComposeFileBuilder {
    constructor(options={}){
        this.dockerComposeRoot = options.dockerComposeRoot||"";
        this.dockerComposeTemplateFilePath = options.dockerComposeTemplateFilePath||"aa";
        this.port = options.port||8080;
    }
    getLocalIP(){
        const ipCollection = [];
        const ipconfig = networkInterfaces();
        const randomIp = ips => {
            let r = Math.ceil((ips.length - 1) * Math.random());
            return ips[r];
        };
        Object.keys(ipconfig).forEach(key => {
            ipconfig[key].forEach(item => {
                if (item.family === "IPv4" && !item.internal) {
                    ipCollection.push(item.address);
                }
            });
        });
        return `${randomIp(ipCollection)}:${this.port}`;
    }
   async generate(){
        if(!this.dockerComposeTemplateFilePath) throw new Error(`You must set the template file location to generate a docker compose file!`);
        if(!this.dockerComposeRoot) throw new Error(`You have not set the docker-compose file template yet!`);
        try{
            let template =await readFileSync(path.resolve(__dirname,this.dockerComposeTemplateFilePath),'utf8');
            template = template.toString().replace('###LOCAL_IP###',this.getLocalIP());
           await writeFileSync(path.resolve(__dirname,this.dockerComposeRoot,'docker-compose.yml'),template,'utf8');
        }catch(error){
            console.log(`Getting error when trying to generate docker compose file
                ${error}
            `)
        }
    }
   async runDockerCompose(){
        try{
            await execFileSync('docker-compose',['up','-d'],{cwd:path.resolve(__dirname,this.dockerComposeRoot)});
        }catch(error){
            console.log(`Getting error when trying to run docker compose!
                ${error}
            `);
        }
    }
    async killDockerCompose(){
        try{
            await execFileSync('docker-compose',['down'],{cwd:path.resolve(__dirname,this.dockerComposeRoot)});
        }catch(error){
            console.log(`Getting error when trying to kill docker compose!
                ${error}
            `);
        }
    }
    async start(){
        try{
            await this.generate();
            await this.killDockerCompose();
            await this.runDockerCompose();
        }catch(error){
            console.log(`Getting error when trying to start our program
                ${error}
            `)
        }
    }
}

module.exports = DockerComposeFileBuilder;